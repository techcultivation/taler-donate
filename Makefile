install_global=false
-include config.mk

.PHONY: all
all:
	@echo "This is a python project, no compilation required"

.PHONY: install

ifeq ($(install_global), true)
install: install-global
else
install: install-local
endif

.PHONY: install-global
install-global:
	pip3 install .

.PHONY: install-local
install-local:
	pip3 install . --user

# run testcases
.PHONY: check
check:
	@export TALER_CONFIG_FILE=talerdonations/tests.conf; \
        python3 setup.py test

.PHONY: clean
clean:
	@echo nothing to do

.PHONY: dist
dist:
	git archive --format=tar.gz HEAD -o taler-merchant-donations.tar.gz

.PHONY: pretty
pretty:
	yapf -r -i talerdonations/
