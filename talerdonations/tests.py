##
# This file is part of GNU TALER.
# Copyright (C) 2014-2016 INRIA
#
# TALER is free software; you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software
# Foundation; either version 2.1, or (at your option) any later version.
#
# TALER is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along with
# GNU TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
#
# @author Marcello Stanisci
# @brief Donations site test case.

#!/usr/bin/env python3

import unittest
from datetime import datetime
from mock import patch, MagicMock
from talerdonations.donations import donations
from taler.util.talerconfig import TalerConfig

TC = TalerConfig.from_env()
CURRENCY = TC["taler"]["currency"].value_string(required=True)


##
# Main class that gathers all the tests.
class DonationsTestCase(unittest.TestCase):
    def setUp(self):
        donations.app.testing = True
        self.app = donations.app.test_client()


if __name__ == "__main__":
    unittest.main()
