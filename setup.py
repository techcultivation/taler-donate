from setuptools import setup, find_packages

setup(
    name='talerdonations',
    version='0.6.0pre1',
    description='Example donations site for GNU Taler',
    url='git://taler.net/donations',
    author=['Marcello Stanisci', 'Florian Dold'],
    author_email=['stanisci.m@gmail.com', 'dold@taler.net'],
    license='GPL',
    packages=find_packages(),
    install_requires=["Flask>=0.10",
                      "requests",
                      "uwsgi",
                      "jsmin",
                      "qrcode",
                      "lxml",
                      "taler-util"],
    tests_require=["mock",
                   "nose"],
    test_suite="nose.collector",
    package_data={
        '': [
            "donations/templates/*.html",
            "donations/static/*.svg",
            "donations/static/*.css",
            "donations/static/web-common/*.png",
            "donations/static/web-common/*.css",
            "donations/static/web-common/*.html",
        ]
    },
    scripts=['./bin/taler-merchant-donations'],
    zip_safe=False
)
