#!/usr/bin/env python3

##
# This file is part of TALER
# (C) 2017 INRIA
#
# TALER is free software; you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public
# License as published by the Free Software Foundation; either
# version 3, or (at your option) any later version.
#
# TALER is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with TALER; see the file COPYING.  If not,
# see <http://www.gnu.org/licenses/>
#
#  @author Florian Dold
#  @file Standalone script to run the donations site.

import argparse
import sys
import os
import site
import logging
from taler.util.talerconfig import TalerConfig

# No perfect match to our logging format, but good enough ...
UWSGI_LOGFMT = "%(ltime) %(proto) %(method) %(uri) %(proto) => %(status)"


##
# This function interprets the 'serve-http' subcommand.
# The effect it to launch the donations HTTP service.
#
# @param args command line options.
def handle_serve_http(args):
    port = args.port
    if port is None:
        port = TC["donations"]["http_port"].value_int(required=True)
    spec = ":%d" % (port, )
    os.execlp(
        "uwsgi", "uwsgi", "--master", "--die-on-term", "--log-format",
        UWSGI_LOGFMT, "--http", spec, "--module", "talerdonations.donations:app"
    )


##
# This function interprets the 'serve-uwsgi' subcommand.
# The effect is to launch the donations UWSGI service.  This
# type of service is usually used when the HTTP donations interface
# is accessed via a reverse proxy (like Nginx, for example).
#
# @param command line options.
def handle_serve_uwsgi(args):
    del args  # pacify PEP checkers
    serve_uwsgi = TC["donations"]["uwsgi_serve"].value_string(required=True
                                                              ).lower()
    params = [
        "uwsgi", "uwsgi", "--master", "--die-on-term", "--log-format",
        UWSGI_LOGFMT, "--module", "talerdonations.donations:app"
    ]
    if serve_uwsgi == "tcp":
        port = TC["donations"]["uwsgi_port"].value_int(required=True)
        spec = ":%d" % (port, )
        params.extend(["--socket", spec])
    elif serve_uwsgi == "unix":
        spec = TC["donations"]["uwsgi_unixpath"].value_filename(required=True)
        mode = TC["donations"]["uwsgi_unixpath_mode"].value_filename(
            required=True
        )
        params.extend(["--socket", spec])
        params.extend(["--chmod-socket=" + mode])
        os.makedirs(os.path.dirname(spec), exist_ok=True)
    logging.info("launching uwsgi with argv %s", params[1:])
    os.execlp(*params)


## @cond
PARSER = argparse.ArgumentParser()
PARSER.set_defaults(func=None)
PARSER.add_argument(
    '--config',
    '-c',
    help="configuration file to use",
    metavar="CONFIG",
    type=str,
    dest="config",
    default=None
)
SUB = PARSER.add_subparsers()

P = SUB.add_parser('serve-http', help="Serve over HTTP")
P.add_argument(
    "--port", "-p", dest="port", type=int, default=None, metavar="PORT"
)
P.set_defaults(func=handle_serve_http)

P = SUB.add_parser('serve-uwsgi', help="Serve over UWSGI")
P.set_defaults(func=handle_serve_uwsgi)

ARGS = PARSER.parse_args()
## @endcond

if getattr(ARGS, 'func', None) is None:
    PARSER.print_help()
    sys.exit(1)

if ARGS.config is not None:
    os.environ["TALER_CONFIG_FILE"] = ARGS.config

TC = TalerConfig.from_file(os.environ.get("TALER_CONFIG_FILE"))
ARGS.func(ARGS)
